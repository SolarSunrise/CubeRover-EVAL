/***************************************************
    L3GD20H Library for the CubeRover, Planetary Robotics
    -----------------------------------------------------
    This is a library for controlling the L3GD20H Gyro
    using any HERCULES microcontroller. It is a heavily
    modified library from Adafruit's L3GD20H/L3GD20
    library.

    To use this library, set up L3GD20H_Status_t with
    appropriate L3GD20H range and its SPI configs, then
    run init_L3GD20H to initialize the gyro.

    December 11th, 2017: File created by Jae Choi
 ****************************************************/

/****************************************************
 * LIBRARY ADAPTED FROM ADAFRUIT FOR THE HERCULES PLATFORM 

    This is a library for the L3GD20H and L3GD20H GYROSCOPE

    Designed specifically to work with the Adafruit L3GD20H(H) Breakout 
    ----> https://www.adafruit.com/products/1032

    These sensors use I2C or SPI to communicate, 2 pins (I2C) 
    or 4 pins (SPI) are required to interface.

    Adafruit invests time and resources providing this open source code,
    please support Adafruit and open-source hardware by purchasing
    products from Adafruit!

    Written by Kevin "KTOWN" Townsend for Adafruit Industries.
    BSD license, all text above must be included in any redistribution
 ****************************************************/

#include "L3GD20H.h"

bool init_L3GD20H(L3GD20H_Status_t *status)
{

    // Make sure that the SPI stuff is all set up!....

    /* Make sure we have the correct chip ID since this checks
         for correct address and that the IC is properly connected */
    uint8_t id = read_reg_L3GD20H(status, L3GD20H_REGISTER_WHO_AM_I);

    if (id != L3GD20H_ID)
    {
        return false;
    }

    /* Set CTRL_REG1 (0x20)
     ====================================================================
     BIT  Symbol    Description                                   Default
     ---  ------    --------------------------------------------- -------
     7-6  DR1/0     Output data rate                                   00
     5-4  BW1/0     Bandwidth selection                                00
         3  PD        0 = Power-down mode, 1 = normal/sleep mode          0
         2  ZEN       Z-axis enable (0 = disabled, 1 = enabled)           1
         1  YEN       Y-axis enable (0 = disabled, 1 = enabled)           1
         0  XEN       X-axis enable (0 = disabled, 1 = enabled)           1 */

    /* Switch to normal mode and enable all three channels */
    write_reg_L3GD20H(status, L3GD20H_REGISTER_CTRL_REG1, 0x0F);
    /* ------------------------------------------------------------------ */

    /* Set CTRL_REG2 (0x21)
     ====================================================================
     BIT  Symbol    Description                                   Default
     ---  ------    --------------------------------------------- -------
     5-4  HPM1/0    High-pass filter mode selection                    00
     3-0  HPCF3..0  High-pass filter cutoff frequency selection      0000 */

    /* Nothing to do ... keep default values */
    /* ------------------------------------------------------------------ */

    /* Set CTRL_REG3 (0x22)
     ====================================================================
     BIT  Symbol    Description                                   Default
     ---  ------    --------------------------------------------- -------
         7  I1_Int1   Interrupt enable on INT1 (0=disable,1=enable)       0
         6  I1_Boot   Boot status on INT1 (0=disable,1=enable)            0
         5  H-Lactive Interrupt active config on INT1 (0=high,1=low)      0
         4  PP_OD     Push-Pull/Open-Drain (0=PP, 1=OD)                   0
         3  I2_DRDY   Data ready on DRDY/INT2 (0=disable,1=enable)        0
         2  I2_WTM    FIFO wtrmrk int on DRDY/INT2 (0=dsbl,1=enbl)        0
         1  I2_ORun   FIFO overrun int on DRDY/INT2 (0=dsbl,1=enbl)       0
         0  I2_Empty  FIFI empty int on DRDY/INT2 (0=dsbl,1=enbl)         0 */

    /* Nothing to do ... keep default values */
    /* ------------------------------------------------------------------ */

    /* Set CTRL_REG4 (0x23)
     ====================================================================
     BIT  Symbol    Description                                   Default
     ---  ------    --------------------------------------------- -------
         7  BDU       Block Data Update (0=continuous, 1=LSB/MSB)         0
         6  BLE       Big/Little-Endian (0=Data LSB, 1=Data MSB)          0
     5-4  FS1/0     Full scale selection                               00
                                                                    00 = 250 dps
                                                                    01 = 500 dps
                                                                    10 = 2000 dps
                                                                    11 = 2000 dps
         0  SIM       SPI Mode (0=4-wire, 1=3-wire)                       0 */

    /* Adjust resolution if requested */
    if (status->range == L3DS20_RANGE_250DPS) {
        write_reg_L3GD20H(status, L3GD20H_REGISTER_CTRL_REG4, 0x00);
    } else if (status->range == L3DS20_RANGE_500DPS) {
        write_reg_L3GD20H(status, L3GD20H_REGISTER_CTRL_REG4, 0x10);
    } else if (status->range == L3DS20_RANGE_2000DPS) {
        write_reg_L3GD20H(status, L3GD20H_REGISTER_CTRL_REG4, 0x20);
    }
    /* ------------------------------------------------------------------ */

    /* Set CTRL_REG5 (0x24)
     ====================================================================
     BIT  Symbol    Description                                   Default
     ---  ------    --------------------------------------------- -------
         7  BOOT      Reboot memory content (0=normal, 1=reboot)          0
         6  FIFO_EN   FIFO enable (0=FIFO disable, 1=enable)              0
         4  HPen      High-pass filter enable (0=disable,1=enable)        0
     3-2  INT1_SEL  INT1 Selection config                              00
     1-0  OUT_SEL   Out selection config                               00 */

    /* Nothing to do ... keep default values */
    /* ------------------------------------------------------------------ */

    return true;
}

/***************************************************************************
 PUBLIC FUNCTIONS
 ***************************************************************************/
void read_data_L3GD20H(L3GD20H_Status_t *status)
{ 
    uint16_t xhi, xlo, ylo, yhi, zlo, zhi;
    uint16_t src;

    status->spiBASE->PC3 &= status->spiDAT1_Config->CSNR;

    src = (uint8_t)(L3GD20H_REGISTER_OUT_X_L | 0x80 | 0x40); // SPI read, autoincrement
    spiTransmitData(status->spiBASE, status->spiDAT1_Config, 1, &src);

    for (int i = 0; i < 100; i++);

    src = 0xFF;
    spiReceiveData(status->spiBASE, status->spiDAT1_Config, 1, &xlo);
    spiReceiveData(status->spiBASE, status->spiDAT1_Config, 1, &xhi);
    spiReceiveData(status->spiBASE, status->spiDAT1_Config, 1, &ylo);
    spiReceiveData(status->spiBASE, status->spiDAT1_Config, 1, &yhi);
    spiReceiveData(status->spiBASE, status->spiDAT1_Config, 1, &zlo);
    spiReceiveData(status->spiBASE, status->spiDAT1_Config, 1, &zhi);

    status->spiBASE->PC3 |= ~(status->spiDAT1_Config->CSNR);

    // Shift values to create properly formed integer (low byte first)
    status->x = (int16_t)(xlo | (xhi << 8));
    status->y = (int16_t)(ylo | (yhi << 8));
    status->z = (int16_t)(zlo | (zhi << 8));
    
    // Compensate values depending on the resolution
    if (status->range == L3DS20_RANGE_250DPS) {
        status->x *= L3GD20H_SENSITIVITY_250DPS;
        status->y *= L3GD20H_SENSITIVITY_250DPS;
        status->z *= L3GD20H_SENSITIVITY_250DPS;
    } else if (status->range == L3DS20_RANGE_500DPS) {
        status->x *= L3GD20H_SENSITIVITY_500DPS;
        status->y *= L3GD20H_SENSITIVITY_500DPS;
        status->z *= L3GD20H_SENSITIVITY_500DPS;
    } else if (status->range == L3DS20_RANGE_2000DPS) {
        status->x *= L3GD20H_SENSITIVITY_2000DPS;
        status->y *= L3GD20H_SENSITIVITY_2000DPS;
        status->z *= L3GD20H_SENSITIVITY_2000DPS;
    }
}

/***************************************************************************
 PRIVATE FUNCTIONS
 ***************************************************************************/
void write_reg_L3GD20H(L3GD20H_Status_t *status, L3GD20H_Registers_t reg, uint8_t value)
{
    uint16_t src;

    // L3GD20H STUFF
    src = (uint8_t) reg;
    status->spiBASE->PC3 &= status->spiDAT1_Config->CSNR;

    spiTransmitData(status->spiBASE, status->spiDAT1_Config, 1, &src);

    src = value;

    spiTransmitData(status->spiBASE, status->spiDAT1_Config, 1, &src);

    status->spiBASE->PC3 |= ~(status->spiDAT1_Config->CSNR);

}

uint8_t read_reg_L3GD20H(L3GD20H_Status_t *status, L3GD20H_Registers_t reg)
{
    uint16_t src;
    uint16_t dest;

    // L3GD20H STUFF
    src = ((uint8_t) reg | 0x80);
    status->spiBASE->PC3 &= status->spiDAT1_Config->CSNR;
    spiTransmitData(status->spiBASE, status->spiDAT1_Config, 1, &src);

    src = 0xFF;
    spiTransmitAndReceiveData(status->spiBASE, status->spiDAT1_Config, 1, &src, &dest);

    status->spiBASE->PC3 |= ~(status->spiDAT1_Config->CSNR);
    
    return (uint8_t) (0xFF & dest);
}
