/** @file sys_main.c 
*   @brief Application main file
*   @date 07-July-2017
*   @version 04.07.00
*
*   This file contains an empty main function,
*   which can be used for the application.
*/

/* 
* Copyright (C) 2009-2016 Texas Instruments Incorporated - www.ti.com 
* 
* 
*  Redistribution and use in source and binary forms, with or without 
*  modification, are permitted provided that the following conditions 
*  are met:
*
*    Redistributions of source code must retain the above copyright 
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the 
*    documentation and/or other materials provided with the   
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


/* USER CODE BEGIN (0) */
/* USER CODE END */

/* Include Files */

#include "sys_common.h"

/* USER CODE BEGIN (1) */
#include "sys_core.h"
#include "rti.h"
#include "sci.h"
#include "spi.h"
#include "gio.h"
#include "drivers/drv8308.h"
#include "drivers/L3GD20H.h"
#include <stdio.h>
/* USER CODE END */

/** @fn void main(void)
*   @brief Application main function
*   @note This function is empty by default.
*
*   This function is called after startup.
*   The user can use this function to implement the application.
*/

/* USER CODE BEGIN (2) */

// SPI Data structure for the DRV8308
spiDAT1_t spiDAT1_DRV8308 = {
    .CS_HOLD = 0,
    .WDEL    = 0,
    .DFSEL   = SPI_FMT_0,
    .CSNR   = SPI_CS_0,
};

// SPI Data structure for the L3GD20H
spiDAT1_t spiDAT1_L3GD20H = {
    .CS_HOLD = 0,
    .WDEL    = 0,
    .DFSEL   = SPI_FMT_0,
    .CSNR   = SPI_CS_0,
};

L3GD20H_Status_t stat;

float integral_x, integral_y, integral_z;
/* USER CODE END */

int main(void)
{
/* USER CODE BEGIN (3) */
    // Initialize peripherals
    gioInit();
    rtiInit();
    spiInit();
    sciInit();

    // Set GPIOA[2] to be an output
    gioSetDirection(gioPORTA, 0x1 << 2U);

    // Set it to HIGH
    gioSetBit(gioPORTA, 2, 1);

    // L3GD20H gyro data structure setup
    stat.spiBASE = spiREG3;
    stat.spiDAT1_Config = &spiDAT1_L3GD20H;
    stat.range = L3DS20_RANGE_250DPS;

    // Initialize L3GD20H gyro
    if (init_L3GD20H(&stat)) sciSend(scilinREG, 9, "Success!\n");
    else sciSend(scilinREG, 6, "FAIL!\n");

    // SETUP HIGH PASS FILTERS FOR THE L3GD20H
    write_reg_L3GD20H(&stat, L3GD20H_REGISTER_CTRL_REG5, 0x12);
    write_reg_L3GD20H(&stat, L3GD20H_REGISTER_CTRL_REG2, 0x20);

    // Enable notification for RTI
    rtiEnableNotification(rtiNOTIFICATION_COMPARE0);

    // Enable IRQ
    _enable_interrupt_();

    // Enable counting
    rtiStartCounter(rtiCOUNTER_BLOCK0);

    // Infinite loop while the RTI interrupt does wonders
    while(true) {}

/* USER CODE END */

    return 0;
}


/* USER CODE BEGIN (4) */

void print_int(int num)
{
    if (num < 0)
    {
        sciSendByte(scilinREG, '-');
        num = -num;
    }
    else if (num == 0)
    {
        sciSendByte(scilinREG, '0');
        return;
    }
    unsigned char A[10];
    int i = 0;
    while (num != 0)
    {
        A[i] = '0'+ (num % 10);
        num /= 10;
        i++;
    }
    for (i--; i >= 0; i--) sciSendByte(scilinREG, A[i]);

}

static void sendfloat (float data)
{

    static int pint;
    static int pfract;
    static char string[7];

    pint =(int)data;
    pfract= (int)((data - pint)*1000);
    pint = abs(pint);
    pfract = abs(pfract);

    sprintf(string,"%03d.%03d",pint,pfract);

    if (data < 0)
    {
        sciSendByte(scilinREG, '-');
    }
    for(int i = 0 ; i < 7 ; i++)
    {
        sciSendByte(scilinREG, string[i]);
    }
}


void rtiNotification(uint32 notification)
{
    // Blink the LED A2
    gioToggleBit(gioPORTA, 2);

    // Source and Destination buffers for SPI transfers
    uint16_t src[1];
    uint16_t dest[1];

    // Read data
    read_data_L3GD20H(&stat);

    sciSend(scilinREG, 3, (unsigned char *) "X: ");
    sendfloat(stat.x);
    sciSend(scilinREG, 14, (unsigned char *) " degrees   Y: ");
    sendfloat(stat.y);
    sciSend(scilinREG, 14, (unsigned char *) " degrees   Z: ");
    sendfloat(stat.z);
    sciSend(scilinREG, 10, (unsigned char *) " degrees\n\r");


    // ********************** DRV8308 STUFF *************************
    src[0] = (DRV8308_WRITE | LOOPGN_REG_ADDR);


    spiREG2->PC3 |= (1U << 0U);
    spiTransmitData(spiREG2, &spiDAT1_DRV8308, 1, src);
    src[0] = 1 << 3U | 1 << 7U;
    spiTransmitData(spiREG2, &spiDAT1_DRV8308, 1, src);
    src[0] = 0x0U;
    spiTransmitData(spiREG2, &spiDAT1_DRV8308, 1, src);
    spiREG2->PC3 &= ~(1 << 0U);

    int i = 0;
    for (i = 0; i < 0x100; i++);

    src[0] = (DRV8308_READ | LOOPGN_REG_ADDR);

    spiREG2->PC3 |= (1U << 0U);
    spiTransmitData(spiREG2, &spiDAT1_DRV8308, 1, src);
    spiReceiveData(spiREG2, &spiDAT1_DRV8308, 1, dest);
    dest[0] = dest[0] << 8;
    spiReceiveData(spiREG2, &spiDAT1_DRV8308, 1, dest);
    spiREG2->PC3 &= ~(1 << 0U);



}
/* USER CODE END */
