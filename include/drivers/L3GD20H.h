/***************************************************
    L3GD20H Library for the CubeRover, Planetary Robotics
    -----------------------------------------------------
    This is a library for controlling the L3GD20H Gyro
    using any HERCULES microcontroller. It is a heavily
    modified library from Adafruit's L3GD20H/L3GD20
    library.

    To use this library, set up L3GD20H_Status_t with
    appropriate L3GD20H range and its SPI configs, then
    run init_L3GD20H to initialize the gyro.

    December 11th, 2017: File created by Jae Choi
 ****************************************************/

/***************************************************
    This is a library for the L3GD20H GYROSCOPE

    Designed specifically to work with the Adafruit L3GD20H Breakout 
    ----> https://www.adafruit.com/products/1032

    These sensors use I2C or SPI to communicate, 2 pins (I2C) 
    or 4 pins (SPI) are required to interface.

    Adafruit invests time and resources providing this open source code,
    please support Adafruit and open-source hardware by purchasing
    products from Adafruit!

    Written by Kevin "KTOWN" Townsend for Adafruit Industries.
    BSD license, all text above must be included in any redistribution
 ****************************************************/
#ifndef __L3GD20H_H__
#define __L3GD20H_H__

#include "spi.h"


#define L3GD20H_ADDRESS                (0x6B)        // 1101011
#define L3GD20H_POLL_TIMEOUT           (100)         // Maximum number of read attempts
#define L3GD20H_ID                     0xD7

#define L3GD20H_SENSITIVITY_250DPS  (0.00875F)      // Roughly 22/256 for fixed point match
#define L3GD20H_SENSITIVITY_500DPS  (0.0175F)       // Roughly 45/256
#define L3GD20H_SENSITIVITY_2000DPS (0.070F)        // Roughly 18/256
#define L3GD20H_DPS_TO_RADS         (0.017453293F)  // degress/s to rad/s multiplier

// **************** REGISTER DEFINITIONS ****************
typedef enum
{                                               // DEFAULT    TYPE
    L3GD20H_REGISTER_WHO_AM_I            = 0x0F,   // 11010111   r
    L3GD20H_REGISTER_CTRL_REG1           = 0x20,   // 00000111   rw
    L3GD20H_REGISTER_CTRL_REG2           = 0x21,   // 00000000   rw
    L3GD20H_REGISTER_CTRL_REG3           = 0x22,   // 00000000   rw
    L3GD20H_REGISTER_CTRL_REG4           = 0x23,   // 00000000   rw
    L3GD20H_REGISTER_CTRL_REG5           = 0x24,   // 00000000   rw
    L3GD20H_REGISTER_REFERENCE           = 0x25,   // 00000000   rw
    L3GD20H_REGISTER_OUT_TEMP            = 0x26,   //            r
    L3GD20H_REGISTER_STATUS_REG          = 0x27,   //            r
    L3GD20H_REGISTER_OUT_X_L             = 0x28,   //            r
    L3GD20H_REGISTER_OUT_X_H             = 0x29,   //            r
    L3GD20H_REGISTER_OUT_Y_L             = 0x2A,   //            r
    L3GD20H_REGISTER_OUT_Y_H             = 0x2B,   //            r
    L3GD20H_REGISTER_OUT_Z_L             = 0x2C,   //            r
    L3GD20H_REGISTER_OUT_Z_H             = 0x2D,   //            r
    L3GD20H_REGISTER_FIFO_CTRL_REG       = 0x2E,   // 00000000   rw
    L3GD20H_REGISTER_FIFO_SRC_REG        = 0x2F,   //            r
    L3GD20H_REGISTER_INT1_CFG            = 0x30,   // 00000000   rw
    L3GD20H_REGISTER_INT1_SRC            = 0x31,   //            r
    L3GD20H_REGISTER_TSH_XH              = 0x32,   // 00000000   rw
    L3GD20H_REGISTER_TSH_XL              = 0x33,   // 00000000   rw
    L3GD20H_REGISTER_TSH_YH              = 0x34,   // 00000000   rw
    L3GD20H_REGISTER_TSH_YL              = 0x35,   // 00000000   rw
    L3GD20H_REGISTER_TSH_ZH              = 0x36,   // 00000000   rw
    L3GD20H_REGISTER_TSH_ZL              = 0x37,   // 00000000   rw
    L3GD20H_REGISTER_INT1_DURATION       = 0x38    // 00000000   rw
} L3GD20H_Registers_t;


// **************** DATA STRUCTURE DEFINITIONS ****************
typedef enum
{
    L3DS20_RANGE_250DPS,
    L3DS20_RANGE_500DPS,
    L3DS20_RANGE_2000DPS
} L3GD20H_Range_t;

typedef struct L3GD20H_status_struct
{
    L3GD20H_Range_t range;
    float x;
    float y;
    float z;
    spiBASE_t *spiBASE;
    spiDAT1_t *spiDAT1_Config;
} L3GD20H_Status_t;


// **************** FUCNTION DEFINITIONS ****************
bool init_L3GD20H(L3GD20H_Status_t *status);
void read_data_L3GD20H(L3GD20H_Status_t *status);
void write_reg_L3GD20H(L3GD20H_Status_t *status, L3GD20H_Registers_t reg, uint8_t value);
uint8_t read_reg_L3GD20H(L3GD20H_Status_t *status, L3GD20H_Registers_t reg);

#endif
