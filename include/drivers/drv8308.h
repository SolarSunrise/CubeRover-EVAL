/* --COPYRIGHT--,BSD
 * Copyright (c) 2013, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

/*******************************************************************************
 *
 *  registers.h - DRV8308 Registers
 *  Nick Oborny
 *  DRV8308EVM_FIRMWARE
 *  1/15/2014
 *
 ******************************************************************************/

#ifndef REGISTERS_DRV8308_H_
#define REGISTERS_DRV8308_H_


#define    CTRL1_REG_ADDR 0x00
#define  ADVANCE_REG_ADDR 0x01
#define COMCTRL1_REG_ADDR 0x02
#define   MOD120_REG_ADDR 0x03
#define    DRIVE_REG_ADDR 0x04
#define  SPDGAIN_REG_ADDR 0x05
#define    FILK1_REG_ADDR 0x06
#define    FILK2_REG_ADDR 0x07
#define   COMPK1_REG_ADDR 0x08
#define   COMPK2_REG_ADDR 0x09
#define   LOOPGN_REG_ADDR 0x0A
#define    SPEED_REG_ADDR 0x0B
//#define    FAULT_REG_ADDR 0x0C
#define    FAULT_REG_ADDR 0x0C


#define DRV8308_WRITE 0x00
#define DRV8308_READ  0x80

// CTRL1 Register
struct CTRL1_Register
{
    unsigned int Address;   // bit 23-16
    unsigned int AG_SETPT;  // bit 15-12
    unsigned int ENPOL;     // bit 11
    unsigned int DIRPOL;    // bit 10
    unsigned int BRKPOL;    // bit 9
    unsigned int SYNRECT;   // bit 8
    unsigned int PWMF;      // bit 7-6
    unsigned int SPDMODE;   // bit 5-4
    unsigned int FGSEL;     // bit 3-2
    unsigned int BRKMOD;    // bit 1
    unsigned int RETRY;     // bit 0
};

// ADVANCE Register
struct ADVANCE_Register
{
    unsigned int Address;   // bit 23-16
    /* Reserved */          // bit 15-8
    unsigned int ADVANCE;   // bit 7-0
};

// COMCTRL1 Register
struct COMCTRL1_Register
{
    unsigned int Address;   // bit 23-16
    unsigned int SPDREVS;   // bit 15-8
    unsigned int MINSPD;    // bit 7-0
};

// MOD120 Register
struct MOD120_Register
{
    unsigned int Address;   // bit 23-16
    unsigned int BASIC;     // bit 15
    unsigned int SPEEDTH;   // bit 14-12
    unsigned int MOD120;    // bit 11-0
};

// DRIVE Register
struct DRIVE_Register
{
    unsigned int Address;   // bit 23-16
    unsigned int LRTIME;    // bit 15-14
    unsigned int HALLRST;   // bit 13-12
    unsigned int DELAY;     // bit 11
    unsigned int AUTOADV;   // bit 10
    unsigned int AUTOGN;    // bit 9
    unsigned int ENSINE;    // bit 8
    unsigned int TDRIVE;    // bit 7-6
    unsigned int DTIME;     // bit 5-3
    unsigned int IDRIVE;    // bit 2-0
};

// SPDGAIN Register
struct SPDGAIN_Register
{
    unsigned int Address;   // bit 23-16
    /* Reserved */          // bit 15
    unsigned int INTCLK;    // bit 14-12
    unsigned int SPDGAIN;   // bit 11-0
};

// FILK1 Register
struct FILK1_Register
{
    unsigned int Address;   // bit 23-16
    unsigned int HALLPOL;   // bit 15
    /* Reserved */          // bit 14-13
    unsigned int BYPFILT;   // bit 12
    unsigned int FILK1;     // bit 11-0

};

// FILK2 Register
struct FILK2_Register
{
    unsigned int Address;   // bit 23-16
    /* Reserved */          // bit 15-12
    unsigned int FILK2;     // bit 11-0

};

// COMPK1 Register
struct COMPK1_Register
{
    unsigned int Address;   // bit 23-16
    /* Reserved */          // bit 15-13
    unsigned int BYPCOMP;   // bit 12
    unsigned int COMPK1;    // bit 11-0
};

// COMPK2 Register
struct COMPK2_Register
{
    unsigned int Address;   // bit 23-16
    unsigned int AA_SETPT;  // bit 15-12
    unsigned int COMPK2;    // bit 11-0
};

// LOOPGN Register
struct LOOPGN_Register
{
    unsigned int Address;   // bit 23-16
    unsigned int OCPDEG;    // bit 15-14
    unsigned int OCPTH;     // bit 13-12
    unsigned int OVTH;      // bit 11
    unsigned int VREF_EN;   // bit 10
    unsigned int LOOPGN;    // bit 9-0
};

// SPEED Register
struct SPEED_Register
{
    unsigned int Address;   // bit 23-16
    /* Reserved */          // bit 15-12
    unsigned int SPEED;     // bit 11-10
};

// FAULT Register
struct FAULT_Register
{
    unsigned int Address;   // bit 23-16
    /* Reserved */          // bit 15-7
    unsigned int RLOCK; // bit 6
    unsigned int VMOV;      // bit 5
    unsigned int CPFAIL;    // bit 4
    unsigned int UVLO;      // bit 3
    unsigned int OTS;       // bit 2
    unsigned int CPOC;      // bit 1
    unsigned int OCP;       // bit 0
};

#endif /* REGISTERS_DRV8308_H_ */
